// listArr.forEach(function(name){
//     let li = document.createElement('li');
//     ul.appendChild(li);
//     li.innerHTML += name;
// });

function addList () {
    let listNumbers = +prompt("How many items you wat to add in the list?", '0');
    let listArr = new Array();
    const ul = document.createElement('ul');
    document.getElementById('myList').appendChild(ul);
    for (let i = 1; i <= listNumbers; i++) {
        listArr.push(prompt(`Enter item\`s name number ${i}`));
    }

    const listContent = listArr.map(name => `<li>${name}</li>`).join(' ');
    const li = document.getElementsByTagName('ul')[0].innerHTML = listContent;

    let countDownDate = new Date().getTime() + 12000;

    let x = setInterval(function () {

        let now = new Date().getTime();
        let distance = countDownDate - now;
        let seconds = Math.floor(distance / 1000);
        document.getElementById("demo").innerHTML = seconds + "s";

        if (distance < 0) {
            clearInterval(x);
            document.getElementById("demo").innerHTML = "EXPIRED";
        }
    }, 1000);

    setTimeout(function () {
        document.getElementsByTagName('ul')[0].innerHTML = "";
    }, 12000);
}